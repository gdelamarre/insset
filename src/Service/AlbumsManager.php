<?php

namespace Service;

use Entity\Album;

class AlbumsManager
{
    public function computeTotalDuration(Album $album)
    {
        $total = 0;

        foreach($album->getSongs() as $song)
        {
            $total += $song->getDuration();
        }

        return $total;
    }

}