<?php

    namespace Utils\Image;

    class Resizer
    {

        public function computeDimensions($width, $height, $maxWidth, $maxHeight, $preserveRatio)
        {
            $computedWidth = 0;
            $computedHeight = 0;

            $originalRatio = $width/$height;

            $heightRatio = 1;
            $widthRatio = 1;

            if($width > $maxWidth)
            {
                $widthRatio = $width/$maxWidth;
            }

            if($height > $maxHeight)
            {
                $heightRatio = $height/$maxHeight;
            }

            if($preserveRatio)
            {
                $widthRatio = $heightRatio = max([$widthRatio, $heightRatio]);
            }

            $computedWidth = $width / $widthRatio;
            $computedHeight = $height / $heightRatio;

            return ['width' => $computedWidth, 'height' => $computedHeight];

        }

    }