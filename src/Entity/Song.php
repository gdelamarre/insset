<?php

namespace Entity;

class Song
{
    /**
     * @var int song duration in seconds
     */
    protected $duration;

    /**
     * @param int $duration
     *
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

}