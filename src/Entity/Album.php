<?php

namespace Entity;

class Album
{

    /**
     * @var array songs part of the album
     */
    protected $songs = [];

    /**
     * @param array $songs
     *
     * @return $this
     */
    public function setSongs($songs)
    {
        $this->songs = $songs;

        return $this;
    }

    /**
     * @return array
     */
    public function getSongs()
    {
        return $this->songs;
    }

}