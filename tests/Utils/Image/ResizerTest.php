<?php

namespace Utils\Image;

class ResizerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Resizer
     */
    protected $resizer;

    public function setUp()
    {
        $this->resizer = new Resizer();
    }

    /**
     * @dataProvider getDataForTestDimensionsAreComputedAccordingToLimitsAndRatio
     */
    public function testDimensionsAreComputedAccordingToLimitsAndRatio($width, $height, $maxWidth, $maxHeight, $preserveRatio, $expected)
    {
        // run
        $result = $this->resizer->computeDimensions($width, $height, $maxWidth, $maxHeight, $preserveRatio);

        // assert
        $this->assertEquals($expected, $result);
    }

    static public function getDataForTestDimensionsAreComputedAccordingToLimitsAndRatio()
    {
        return [
            [200, 100, 100, 100, true, ['width' => 100, 'height' => 50]],
            [200, 100, 100, 100, false, ['width' => 100, 'height' => 100]],
            [200, 300, 100, 100, false, ['width' => 100, 'height' => 100]],
            [200, 300, 100, 100, true, ['width' => 200/3, 'height' => 100]],
            [50, 200, 100, 100, false, ['width' => 50, 'height' => 100]],
            [50, 200, 100, 100, true, ['width' => 25, 'height' => 100]],
            [50, 43, 100, 100, true, ['width' => 50, 'height' => 43]],
            [50, 43, 100, 100, false, ['width' => 50, 'height' => 43]],

        ];
    }

}

