<?php

namespace Service;

class AlbumsManagerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var AlbumsManager
     */
    protected $manager;

    public function setUp()
    {
        $this->manager = new AlbumsManager();
    }


    public function testTotalDurationComputation()
    {

        // prepare
        $songs = [];
        $songs[] = $this->getSongMockForDurationComputation(213);
        $songs[] = $this->getSongMockForDurationComputation(322);
        $songs[] = $this->getSongMockForDurationComputation(184);
        $songs[] = $this->getSongMockForDurationComputation(100);

        $album = $this->getMock('Entity\Album');
        $album->expects($this->once())->method('getSongs')->will($this->returnValue($songs));

        // run
        $duration = $this->manager->computeTotalDuration($album);

        // assert
        $this->assertEquals(213 + 322 + 184 + 100, $duration);

    }

    protected function getSongMockForDurationComputation($duration)
    {
        $mock = $this->getMock('Entity\Song');

        $mock->expects($this->once())->method('getDuration')->will($this->returnValue($duration));

        return $mock;
    }

}